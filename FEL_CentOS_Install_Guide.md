__Installation Guide for Duke FEL Lab Cenots 7 Computers__

# Prepare Installation Media 

We first build a netinstall OS (even smaller than minimal install OS) on
a USB drive. This iso file can be downloaded from:
```
http://packages.oit.ncsu.edu/centos/7.6.1810/isos/x86_64/CentOS-7-x86_64-NetInstall-1810.iso
```
Then write to the USB 
```
dd if=/path/to/CentOS-7-x86_64-NetInstall-1810.iso of=/dev/sdb bs=1M
```
where `/dev/sdb` is the USB device. 

# Install CentOS 7

Boot computer, set `UEFI Boot` in BIOS, and choose `boot from UEFI USB`
in booting device list.

## Network

First check the vlan of this ethernet port, connecting a working
computer to it and use the following command: 
```
tcpdump -nn -v -i p3p1 -s 1500 -c 1 'ether[20:2] == 0x2000'
```
where `p3p1` is the ethernet device name.
Here is an example output: 
```
tcpdump: listening on p3p1, link-type EN10MB (Ethernet), capture size 1500 bytes
17:11:55.303929 CDPv2, ttl: 180s, checksum: 0x79c1 (unverified), length 468
	Device-ID (0x01), value length: 29 bytes: 'fel-106-a-2-1.netcom.duke.edu'
	Version String (0x05), value length: 244 bytes: 
	  Cisco IOS Software, C3750 Software (C3750-IPBASEK9-M), Version 12.2(55)SE10, RELEASE SOFTWARE (fc2)
	  Technical Support: http://www.cisco.com/techsupport
	  Copyright (c) 1986-2015 by Cisco Systems, Inc.
	  Compiled Wed 11-Feb-15 11:40 by prod_rel_team
	Platform (0x06), value length: 18 bytes: 'cisco WS-C3750-48P'
	Address (0x02), value length: 13 bytes: IPv4 (1) 10.140.3.210
	Port-ID (0x03), value length: 18 bytes: 'FastEthernet4/0/42'
	Capability (0x04), value length: 4 bytes: (0x00000028): L2 Switch, IGMP snooping
	Protocol-Hello option (0x08), value length: 32 bytes: 
	VTP Management Domain (0x09), value length: 13 bytes: 'fel-106-a-2-1'
	Native VLAN ID (0x0a), value length: 2 bytes: 611
	Duplex (0x0b), value length: 1 byte: full
	ATA-186 VoIP VLAN request (0x0e), value length: 3 bytes: app 1, vlan 101
	AVVID trust bitmap (0x12), value length: 1 byte: 0x00
	AVVID untrusted ports CoS (0x13), value length: 1 byte: 0x00
	Management Addresses (0x16), value length: 13 bytes: IPv4 (1) 10.140.3.210
	unknown field type (0x1a), value length: 12 bytes: 
	  0x0000:  0000 0001 0000 0000 ffff ffff
1 packet captured
1 packet received by filter
0 packets dropped by kernel
```
`fel-106-a-2-1` is the switch name.  `FastEthernet4/0/42` is the
interface name. `611` is the vlan ID of this interface. There are
4 vlans for FEL network:  
```
10.236.57.0/24  Physics-FEL-VLAN610
10.236.58.0/24  Physics-FEL-VLAN611
10.236.59.0/24  Physics-FEL-VLAN612
152.3.54.0/24   Physics FELL (152.3.54.0/24)
```
Their information can be found at `cartographer.oit.duke.edu`. The
vlan `611` means the computers attached to this interface has to be
assigned an IP within `10.236.58.0/24`. We can change the vlan of the
interface at `cartographer.oit.duke.edu`: 
1. Search `fel-106-a-2-1` at `cartographer.oit.duke.edu`.
2. Click `Interfaces` for this device. 
3. Click `FastEthernet4/0/42`.
4. Click `Edit Interface` and change the vlan.

After the ethernet port is configured, we need to add this computer to
Duke DNS service. This work can be done through `aka.oit.duke.edu`:
1. Search `10.236.58` in `aka.oit.duke.edu`
2. Click subnet `10.236.58.0/24`. It will list all the DNS names and
   IPs of the computers in this subnet. 
3. Find a vacant IP and click `Add Net Entry` on top-right corner
4. Input a name (e.g. control-spare-01.fel.duke.edu), the IP address
   (e.g. 10.236.58.61), and the hardware address of this computer. 

After configuring the network settings at Duke, we can assign the
above IP address in `IPv4 Settings`and DNS name to the computer at
CentOS installation interface, here is a summary:
```
Host name: control-spare-01.fel.duke.edu
Method: Manual
Address: 10.236.58.61
Netmask: 255.255.255.0
Gateway: 10.236.58.1
DNS server: 152.3.70.100
Search domain: fel.duke.edu
```
Then turn on the network. 

## Installation Destination 
Choose `I will configure the partitioning`, and press `Done`. Then
erase the hard drive, and choose `Standard partition`. Then add 3
partitions. Here is an example: 
```
/boot/efi 500 MiB
/ 100 GiB, ext4 
swap 20 GiB
```

## Installation Source
Use the following repository (as of 8/6/2019)
```
http://mirror.linux.duke.edu/centos/7.6.1810/os/linux86_64
``` 

## Software Selection
Choose `GNOME Desktop`, and the `GNOME Applications` and `Office Suite
and Productivity`. 


## Kdump
Disable it.

## User setup
Set up `root` and `test` users. The `test` user home directory is
`/opt/home/test`, and add it to `wheel` group. 

# Post-install, Basic Configuration

## Establish no password ssh
Copy the public key of the user from Ansible host to `test` user
account on Ansible guest, this key is named as `authorized_keys`,
e.g. on Ansible host:
```
scp ansible-host:~/.ssh/id_rsa.pub ansible-guest:~test/.ssh/authorized_keys
```
Then we can `ssh test@ansible-guest` without password from the current
user on Ansible host. 


## sudoer
The user `test` has been added to `wheel` group, so it is already a
sudoer. Let us turn off the sudo password requirement for `test`. Edit
`/etc/sudoer`: remove the comment `#` for this line (keep `%`),
```
%wheel	ALL=(ALL)	NOPASSWD: ALL
```

## Install necessary software
```
sudo mount -t nfs daffy:/export/system/cfg/linux /mnt
cd /mnt
sudo ./post_install_CentOS7.6 epics|workstation
```
The above script set up the necessary settings and install
software. This script will be replaced by the `ansible` program.

# Post-install, Duke Physics/FEL Configuration

## ansible-playbook (push) on host computer

We first use `ansible-playbook` to push install `authdir` on Ansible
host. All the configuration files are host on
`gitlab.oit.duke.edu`. After grabbing these configurations, edit the
`ansible_hosts-hao`, change the entry under `[felnext]` to the Ansible
guest. This can be done as a regular user at Ansible host:

```
git clone git@gitlab.oit.duke.edu:Physics/physics_ansible.git or git pull origin
cd physics_ansible
edit/add ansible_hosts-hao, modify [felnext] entry
ansible-playbook -i ansible_hosts-hao  --extra-vars "rem_user=test" -l felnext authdir-setup-nfshome.yml --ask-vault-pass
ansible -i ansible_hosts-hao felnext -m shell -a "service sssd restart" -u test --become
```
Password is needed for `ansible-playbook`. 

Then install the physics ansible cron: 
```
ansible -i ansible_hosts-hao felnext -m shell -a "yum -y install http://install.phy.duke.edu/rpms/ans-0.3-1.noarch.rpm" -u test --become
```

## ansible-pull on target computer

Login to the target computer, first modify the last line `/etc/cron.daily/ans-run.sh` to 
```
/usr/bin/ansible-pull -U https://gitlab.oit.duke.edu/hh62/fel_ansible_pull.git
```
This git repository is from
`https://gitlab.oit.duke.edu/jdorff/physics-ansible-pull.git`, but
with FEL computers. 

Then we can use `ansible-pull` at Ansible guest to set up and install
the software, and configure the system. This command uses a git
repository, and we must prepare a dedicated configuration file for
this particular computer in this git repository. This config file's
name is the same as the hostname, e.g. `control-spare-01.yml`, and the
repository is `https://gitlab.oit.duke.edu/hh62/fel_ansible_pull.git`
(we can change it to whatever we want):
```
git clone https://gitlab.oit.duke.edu/hh62/fel_ansible_pull.git, or git pull
edit/add control-spare-01.yml, it is the same as other files
git add control-spare-01.yml
git commit
git push
```
Then we need to add the Ansible guest to the ansible hosts file, 
```
sudo /usr/bin/hostname --long >> /etc/ansible/hosts
```
Then we can use `ansible-pull` and this git repository to configure
the computer:
```
sudo /usr/bin/ansible-pull -U https://gitlab.oit.duke.edu/hh62/fel_ansible_pull.git
```
