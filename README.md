__A good cheat sheet about using git__

* [SSH key](#ssh-key)
* [.git directory](#git-directory)
* [Basic git commands](#basic-git-commands)
* [git config](#git-config)
* [git add](#git-add)
* [git commit](#git-commit)
* [git clone](#git-clone)
* [git push](#git-push)
* [git pull](#git-pull)
* [git rm](#git-rm)
* [git branch](#git-branch)
* [git checkout](#git-checkout)
* [git status](#git-status)
* [git log](#git-log)
* [git diff](#git-diff)
* [git fetch](#git-fetch)
* [git remote](#git-remote)
* [git reset](#git-reset)
* [Practical procedures](#practical-procedures)
* [Set up git for an existing non-git project](#set-up-git-for-an-existing-non-git-project)
* [Add a folder and its files to git](#add-a-folder-and-its-files-to-git)
* [Delete git from current project (keeping all files)](#delete-git-from-current-project-keeping-all-files)
* [Push an existing non-git folder to remote repository (gitlab.oit.duke.edu)](#push-an-existing-non-git-folder-to-remote-repository-gitlaboitdukeedu)
* [Push an existing git folder to remote repository (gitlab.oit.duke.edu)](#push-an-existing-git-folder-to-remote-repository-gitlaboitdukeedu)
* [Add another person to the remote repository](#add-another-person-to-the-remote-repository)

# SSH key setup on gitlab.oit.duke.edu

ssh key is important for password-less access to private projects on
`gitlab.oit.duke.edu`.

Generate ssh key pair
```
ssh-keygen -t ed25519 -C "hh62@duke.edu"
```
where `-C` is the comment in the public key.

There will be 2 new files generated in `~/.ssh`, with names `id_ed25519` and
`id_ed25519.pub`
```
haohao@Haos-iMac:~/Documents/proj01$ ls -l ~/.ssh
total 24
-rw-------  1 haohao  staff   399 Jul 18 23:44 id_ed25519
-rw-r--r--  1 haohao  staff    95 Jul 18 23:44 id_ed25519.pub
-rw-r--r--  1 haohao  staff  2815 Jul 18 23:56 known_hosts
```
where `id_ed25519` is the private key, which should never be shared with or
seen by other people; `id_ed25519.pub` is the public key, which can be used anywhere.

Next copy the public key to `gitlab.oit.duke.edu`: Top-right corner
`Settings`, `SSH Keys`, then paste content of the
`id_ed25519.pub`. You should receive an email regarding this ssh key.

You can test it using
```
ssh -T git@gitlab.oit.duke.edu
```
You will see something like this, without asking you the password.
```
haohao@Haos-iMac:~/Documents/proj01$ ssh -T git@gitlab.oit.duke.edu
################################################################################
# You are about to access a Duke University computer network that is intended  #
# for authorized users only. You should have no expectation of privacy in      #
# your use of this network. Use of this network constitutes consent to         #
# monitoring, retrieval, and disclosure of any information stored within the   #
# network for any purpose including criminal prosecution.                      #
################################################################################
Welcome to GitLab, @hh62!
```

# .git directory

Then you can `git pull` from or `git push` to the remote repository
`gitlab.oit.duke.edu` without using the password.

We can set up an empty project on `gitlab.oit.duke.edu`, called
`proj01`. Then we can `git clone` this project to local computer:
```
git clone git@gitlab.oit.duke.edu:hh62/proj01.git
```
or
```
git clone https://hh62@gitlab.oit.duke.edu/hh62/proj01.git
```
There will be a new folder named as `proj01` generated at the current
directory. This new folder is "empty":
```
haohao@Haos-iMac:~/Documents/proj01$ tree -a
.
├── .git
│   ├── COMMIT_EDITMSG
│   ├── HEAD
│   ├── config
│   ├── description
│   ├── hooks
│   │   ├── applypatch-msg.sample
│   ├── index
│   ├── info
│   │   └── exclude
│   ├── logs
│   │   ├── HEAD
│   │   └── refs
│   │       ├── heads
│   │       │   └── master
│   │       └── remotes
│   │           └── origin
│   │               └── master
│   ├── objects
│   │   ├── 05
│   │   │   └── c6d779a4426c9e5ff035359b75381c95caf58b
│   │   ├── info
│   │   └── pack
│   └── refs
│       ├── heads
│       │   └── master
│       ├── remotes
│       │   └── origin
│       │       └── master
│       └── tags
```
where there is only a `.git` directory in it. This hidden directory is
a standalone database containing ALL information for the version
control, including the old version files.

The `config` file contains the configuration information about this project:
```
haohao@Haos-iMac:~/Documents/proj01/.git$ cat config
[core]
	repositoryformatversion = 0
	filemode = true
	bare = false
	logallrefupdates = true
	ignorecase = true
	precomposeunicode = true
[remote "origin"]
	url = https://hh62@gitlab.oit.duke.edu/hh62/proj01.git
	fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
	remote = origin
	merge = refs/heads/master
```
where `origin` is the name of the remote repository, which is https://hh62@gitlab.oit.duke.edu/hh62/proj01.git, `master` is the name of the branch where the local workspace is working on.


# Basic git commands
## git config
Set the global user.name, which is default for all projects.
```
git config --global user.name "hh62"
git config --global user.name "hh62@duke.edu"
git config --global core.editor "nano"
```
These global information are stored at `~/.gitconfig`.

We can also set the configuration for particular project in its local workspace:
```
git config user.name "Hao Hao"
```
It will not overwrite the global configuration, but added to the end of the `git config --list` output.

If you run `git config --list` in a non-git directory, you can only list the global information
```
haohao@Haos-iMac:~$ git config --list
credential.helper=osxkeychain
user.name=hh62@duke.edu
user.email=diverhao@gmail.com
color.ui=auto
core.editor=nano
```
But if you run it in a git project, you will get the local information
```
haohao@Haos-iMac:~/Documents/proj01$ git config --list
credential.helper=osxkeychain
user.name=hh62@duke.edu
user.email=diverhao@gmail.com
color.ui=auto
core.editor=nano
core.repositoryformatversion=0
core.filemode=true
core.bare=false
core.logallrefupdates=true
core.ignorecase=true
core.precomposeunicode=true
remote.origin.url=https://hh62@gitlab.oit.duke.edu/hh62/proj01.git
remote.origin.fetch=+refs/heads/*:refs/remotes/origin/*
branch.master.remote=origin
branch.master.merge=refs/heads/master
```

## git add

When we added a new file/folder to the workspace, or edited an existing file, we need to use `git add` to add these new/modified files to
staging area.  
```
hh62@electron:~/study/git/proj01$ git add tst01 tst02
```
where tst01 is a directory, and tst02 is a file. The above command adds all files in the tst01 dir and the file tst02 to staging area.


## git commit

After `git add`, the file is in the staging area, not in the local repository yet, We need `git commit` to really add these files to 
version control.
```
git commit -m "This is the comment for the commit"
```
where `-m` is for the comment. We can also ignore this option, then the `core.editor` in `git config` will be prompt for the comment.



## git clone
## git push

Set the default `origin` as the default remote. 
```
git push -u origin master
```


## git pull
`git pull` is the combination of `git fetch` and `git merge`

## git rm
## git branch
## git checkout
## git status


## git log
## git diff
## git fetch
## git remote

A git project can have multiple remote 

Show the remote names: 
```
haohao@Haos-iPadPro129:~/proj01$ git remote -v 
origin  git@gitlab.oit.duke.edu:hh62/proj01.git (fetch)
origin  git@gitlab.oit.duke.edu:hh62/proj01.git (push)
```
The `origin` the name of the remote, it shows in commands like `git push|pull origin master`. 


Rename the remote name from `orgin` to `newName`
```
git remote rename origin newName
```

A git project can have multiple remotes, there is one default, which can be set using `git push -u origin master` command. With this default remote, we can simply `git push|pull` without using the `origin` or `master` arguments.  Now let us add a new remote
```
get remote add newRemoteName git@gitlab.oit.duke.edu:hh62/proj01.git
```
Note that for a blank workspace, we need `git init` to initialize the project. This command only add a new remote to the git configuration. It does not `pull` or `fetch` the data. Then we need `git pull newRemoteName master` to get the data. 


## git reset

# Practical procedures
## Set up git for an existing non-git project
## Add a folder and its files to git
## Delete git from current project (keeping all files)
## Push an existing non-git folder to remote repository (gitlab.oit.duke.edu)
## Push an existing git folder to remote repository (gitlab.oit.duke.edu)
## Add another person to the remote repository

# References

* http://www.ruanyifeng.com/blog/2015/12/git-cheat-sheet.html
* https://git-scm.com/book/en/v2

 1213  
 1214  llt
 1215  cd proj01/
 1216  ls
 1217  cd ..
 1218  cd proj01/
 1219  ls
 1220  cd ..
 1221  rm -rf proj01/
 1222  git clone https://gitlab.oit.duke.edu/hh62/proj01.git
 1223  git clone hh62@https://gitlab.oit.duke.edu/hh62/proj01.git
 1224  git clone hh62@https://gitlab.oit.duke.edu/hh62/proj01.git
 1225  rm -rf proj01/
 1226  git clone hh62@https://gitlab.oit.duke.edu/hh62/proj01.git
 1227  git clone https://hh62@gitlab.oit.duke.edu/hh62/proj01.git
 1228  llt
 1229  rm -rf proj01/
 1230  git clone https://hh62@gitlab.oit.duke.edu/hh62/proj01.git
 1231  llt
 1232  rm -rf proj01/
 1233*
 1234  git clone https://hh62@gitlab.oit.duke.edu/hh62/proj01.git
 1235  llt
 1236  cd proj01/
 1237  l
 1238  git config --list
 1242  touch README.md
 1243  git add README.md
 1244  git commit -m "add README"
 1245  git status
 1246  git push -u origin master
 1247  grep -r origin *
 1248  grep -r origin .*
 1249  cd .git/
 1250  grep -r origin *
 1251  cat config
 1252  cd ..
 1253  ls
 1254  emacs README.md &
 1255  history
haohao@Haos-iMac:~/Documents/proj01$
